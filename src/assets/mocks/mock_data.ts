const mockData = {
    "pagination": {
        "total": 125907,
        "limit": 8,
        "offset": 0,
        "total_pages": 15739,
        "current_page": 1,
        "next_url": "https://api.artic.edu/api/v1/artworks?page=2&limit=8"
    },
    "data": [
        {
            "id": 102472,
            "api_model": "artworks",
            "api_link": "https://api.artic.edu/api/v1/artworks/102472",
            "is_boosted": false,
            "title": "Oriental Theatre, Milwaukee, Wisconsin",
            "alt_titles": null,
            "thumbnail": {
                "lqip": "data:image/gif;base64,R0lGODlhBAAFAPQAAB4jKiYnLzQqMDo6PFIxNT89QUFBRkhESElFSU1ISnJKSWNZVXxUUolOTIlSTIpiWJdyXKZ1VaB0XI1waAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAAAAAAALAAAAAAEAAUAAAUR4HIUCRIAgyEQCtM4zyRFUAgAOw==",
                "width": 2406,
                "height": 3000,
                "alt_text": "A work made of chromogenic print."
            },
            "main_reference_number": "1984.417",
            "has_not_been_viewed_much": true,
            "boost_rank": null,
            "date_start": 1983,
            "date_end": 1983,
            "date_display": "1983",
            "date_qualifier_title": "Made",
            "date_qualifier_id": 4,
            "artist_display": "Don A. DuBroff\nAmerican, born 1951",
            "place_of_origin": "United States",
            "description": null,
            "short_description": null,
            "dimensions": "Image: 48.1 × 38 cm (18 15/16 × 15 in.); Paper: 50.7 × 40.6 cm (20 × 16 in.)",
            "dimensions_detail": [
                {
                    "depth": null,
                    "width": 38,
                    "height": 48,
                    "diameter": null,
                    "clarification": "Image"
                },
                {
                    "depth": null,
                    "width": 40,
                    "height": 50,
                    "diameter": null,
                    "clarification": "Paper"
                }
            ],
            "medium_display": "Chromogenic print",
            "inscriptions": null,
            "credit_line": "Purchased with funds provided by Jonas Dovydenas",
            "catalogue_display": null,
            "publication_history": null,
            "exhibition_history": null,
            "provenance_text": null,
            "edition": null,
            "publishing_verification_level": "Web Basic",
            "internal_department_id": 20,
            "fiscal_year": null,
            "fiscal_year_deaccession": null,
            "is_public_domain": false,
            "is_zoomable": false,
            "max_zoom_window_size": 843,
            "copyright_notice": null,
            "has_multimedia_resources": false,
            "has_educational_resources": false,
            "has_advanced_imaging": false,
            "colorfulness": 41.467,
            "color": {
                "h": 339,
                "l": 50,
                "s": 56,
                "percentage": 0.002483089595515992,
                "population": 22
            },
            "latitude": null,
            "longitude": null,
            "latlon": null,
            "is_on_view": false,
            "on_loan_display": null,
            "gallery_title": null,
            "gallery_id": null,
            "nomisma_id": null,
            "artwork_type_title": "Photograph",
            "artwork_type_id": 2,
            "department_title": "Photography and Media",
            "department_id": "PC-12",
            "artist_id": 30979,
            "artist_title": "Don A. DuBroff",
            "alt_artist_ids": [],
            "artist_ids": [
                30979
            ],
            "artist_titles": [
                "Don A. DuBroff"
            ],
            "category_ids": [
                "PC-12"
            ],
            "category_titles": [
                "Photography and Media"
            ],
            "term_titles": [
                "chromogenic color print",
                "photographic process",
                "photography",
                "photograph"
            ],
            "style_id": null,
            "style_title": null,
            "alt_style_ids": [],
            "style_ids": [],
            "style_titles": [],
            "classification_id": "TM-558",
            "classification_title": "chromogenic color print",
            "alt_classification_ids": [
                "TM-68",
                "TM-67",
                "TM-240"
            ],
            "classification_ids": [
                "TM-558",
                "TM-68",
                "TM-67",
                "TM-240"
            ],
            "classification_titles": [
                "chromogenic color print",
                "photographic process",
                "photography",
                "photograph"
            ],
            "subject_id": null,
            "alt_subject_ids": [],
            "subject_ids": [],
            "subject_titles": [],
            "material_id": null,
            "alt_material_ids": [],
            "material_ids": [],
            "material_titles": [],
            "technique_id": null,
            "alt_technique_ids": [],
            "technique_ids": [],
            "technique_titles": [],
            "theme_titles": [],
            "image_id": "acebe62e-e7d2-b1bf-49bc-dc0324b16dde",
            "alt_image_ids": [],
            "document_ids": [],
            "sound_ids": [],
            "video_ids": [],
            "text_ids": [],
            "section_ids": [],
            "section_titles": [],
            "site_ids": [],
            "suggest_autocomplete_all": [
                {
                    "input": [
                        "1984.417"
                    ],
                    "contexts": {
                        "groupings": [
                            "accession"
                        ]
                    }
                },
                {
                    "input": [
                        "Oriental Theatre, Milwaukee, Wisconsin"
                    ],
                    "weight": 17,
                    "contexts": {
                        "groupings": [
                            "title"
                        ]
                    }
                }
            ],
            "source_updated_at": "2022-09-21T19:19:06-05:00",
            "updated_at": "2024-06-30T23:28:52-05:00",
            "timestamp": "2024-07-01T00:23:24-05:00"
        },
        {
            "id": 102471,
            "api_model": "artworks",
            "api_link": "https://api.artic.edu/api/v1/artworks/102471",
            "is_boosted": false,
            "title": "Oriental Theatre, Milwaukee, Wisconsin",
            "alt_titles": null,
            "thumbnail": {
                "lqip": "data:image/gif;base64,R0lGODlhBAAFAPQAAB4jKiYnLzQqMDo6PFIxNT89QUFBRkhESElFSU1ISnJKSWNZVXxUUolOTIlSTIpiWJdyXKZ1VaB0XI1waAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAAAAAAALAAAAAAEAAUAAAUR4HIUCRIAgyEQCtM4zyRFUAgAOw==",
                "width": 2406,
                "height": 3000,
                "alt_text": "A work made of chromogenic print."
            },
            "main_reference_number": "1984.416",
            "has_not_been_viewed_much": true,
            "boost_rank": null,
            "date_start": 1983,
            "date_end": 1983,
            "date_display": "1983",
            "date_qualifier_title": "Made",
            "date_qualifier_id": 4,
            "artist_display": "Don A. DuBroff\nAmerican, born 1951",
            "place_of_origin": "United States",
            "description": null,
            "short_description": null,
            "dimensions": "Image: 48.1 × 38 cm (18 15/16 × 15 in.); Paper: 50.7 × 40.6 cm (20 × 16 in.)",
            "dimensions_detail": [
                {
                    "depth": null,
                    "width": 38,
                    "height": 48,
                    "diameter": null,
                    "clarification": "Image"
                },
                {
                    "depth": null,
                    "width": 40,
                    "height": 50,
                    "diameter": null,
                    "clarification": "Paper"
                }
            ],
            "medium_display": "Chromogenic print",
            "inscriptions": null,
            "credit_line": "Purchased with funds provided by Reva and David Logan and Illinois Arts Council Matching Grant",
            "catalogue_display": null,
            "publication_history": null,
            "exhibition_history": null,
            "provenance_text": null,
            "edition": null,
            "publishing_verification_level": "Web Basic",
            "internal_department_id": 20,
            "fiscal_year": null,
            "fiscal_year_deaccession": null,
            "is_public_domain": false,
            "is_zoomable": false,
            "max_zoom_window_size": 843,
            "copyright_notice": null,
            "has_multimedia_resources": false,
            "has_educational_resources": false,
            "has_advanced_imaging": false,
            "colorfulness": 41.467,
            "color": {
                "h": 339,
                "l": 50,
                "s": 56,
                "percentage": 0.002483089595515992,
                "population": 22
            },
            "latitude": null,
            "longitude": null,
            "latlon": null,
            "is_on_view": false,
            "on_loan_display": null,
            "gallery_title": null,
            "gallery_id": null,
            "nomisma_id": null,
            "artwork_type_title": "Photograph",
            "artwork_type_id": 2,
            "department_title": "Photography and Media",
            "department_id": "PC-12",
            "artist_id": 30979,
            "artist_title": "Don A. DuBroff",
            "alt_artist_ids": [],
            "artist_ids": [
                30979
            ],
            "artist_titles": [
                "Don A. DuBroff"
            ],
            "category_ids": [
                "PC-12"
            ],
            "category_titles": [
                "Photography and Media"
            ],
            "term_titles": [
                "chromogenic color print",
                "photographic process",
                "photography",
                "photograph"
            ],
            "style_id": null,
            "style_title": null,
            "alt_style_ids": [],
            "style_ids": [],
            "style_titles": [],
            "classification_id": "TM-558",
            "classification_title": "chromogenic color print",
            "alt_classification_ids": [
                "TM-68",
                "TM-67",
                "TM-240"
            ],
            "classification_ids": [
                "TM-558",
                "TM-68",
                "TM-67",
                "TM-240"
            ],
            "classification_titles": [
                "chromogenic color print",
                "photographic process",
                "photography",
                "photograph"
            ],
            "subject_id": null,
            "alt_subject_ids": [],
            "subject_ids": [],
            "subject_titles": [],
            "material_id": null,
            "alt_material_ids": [],
            "material_ids": [],
            "material_titles": [],
            "technique_id": null,
            "alt_technique_ids": [],
            "technique_ids": [],
            "technique_titles": [],
            "theme_titles": [],
            "image_id": "acebe62e-e7d2-b1bf-49bc-dc0324b16dde",
            "alt_image_ids": [],
            "document_ids": [],
            "sound_ids": [],
            "video_ids": [],
            "text_ids": [],
            "section_ids": [],
            "section_titles": [],
            "site_ids": [],
            "suggest_autocomplete_all": [
                {
                    "input": [
                        "1984.416"
                    ],
                    "contexts": {
                        "groupings": [
                            "accession"
                        ]
                    }
                },
                {
                    "input": [
                        "Oriental Theatre, Milwaukee, Wisconsin"
                    ],
                    "weight": 29,
                    "contexts": {
                        "groupings": [
                            "title"
                        ]
                    }
                }
            ],
            "source_updated_at": "2022-09-21T19:19:06-05:00",
            "updated_at": "2024-06-30T23:28:52-05:00",
            "timestamp": "2024-07-01T00:23:24-05:00"
        },
        {
            "id": 102470,
            "api_model": "artworks",
            "api_link": "https://api.artic.edu/api/v1/artworks/102470",
            "is_boosted": false,
            "title": "Our Lady of Sorrows Basilica, Chicago",
            "alt_titles": null,
            "thumbnail": {
                "lqip": "data:image/gif;base64,R0lGODlhBgAFAPQAAFlUS2ddVGRiVm1kWn1nVX5mWHJoX2lsam97fXJyc39/foBuVoRpWYRtXYZ1W4BwX4JybYJ4bJV9YYd4dYp7cYF4eJSBc5+Kc5aNe5mOfaCLa6OLdMCZbsKdcgAAAAAAACH5BAAAAAAALAAAAAAGAAUAAAUYoAJNVYJQBdMYh0UEwxNtwOIIVyZxnYaFADs=",
                "width": 3000,
                "height": 2384,
                "alt_text": "A work made of chromogenic print."
            },
            "main_reference_number": "1984.415",
            "has_not_been_viewed_much": true,
            "boost_rank": null,
            "date_start": 1981,
            "date_end": 1981,
            "date_display": "1981",
            "date_qualifier_title": "Made",
            "date_qualifier_id": 4,
            "artist_display": "Don A. DuBroff\nAmerican, born 1951",
            "place_of_origin": "United States",
            "description": null,
            "short_description": null,
            "dimensions": "Image: 38 × 48.2 cm (15 × 19 in.); Paper: 40.6 × 50.7 cm (16 × 20 in.)",
            "dimensions_detail": [
                {
                    "depth": null,
                    "width": 48,
                    "height": 38,
                    "diameter": null,
                    "clarification": "Image"
                },
                {
                    "depth": null,
                    "width": 50,
                    "height": 40,
                    "diameter": null,
                    "clarification": "Paper"
                }
            ],
            "medium_display": "Chromogenic print",
            "inscriptions": null,
            "credit_line": "Purchased with funds provided by Jonas Dovydenas",
            "catalogue_display": null,
            "publication_history": null,
            "exhibition_history": null,
            "provenance_text": null,
            "edition": null,
            "publishing_verification_level": "Web Basic",
            "internal_department_id": 20,
            "fiscal_year": null,
            "fiscal_year_deaccession": null,
            "is_public_domain": false,
            "is_zoomable": false,
            "max_zoom_window_size": 843,
            "copyright_notice": null,
            "has_multimedia_resources": false,
            "has_educational_resources": false,
            "has_advanced_imaging": false,
            "colorfulness": 36.2933,
            "color": {
                "h": 38,
                "l": 51,
                "s": 57,
                "percentage": 0.002832811033798977,
                "population": 16
            },
            "latitude": null,
            "longitude": null,
            "latlon": null,
            "is_on_view": false,
            "on_loan_display": null,
            "gallery_title": null,
            "gallery_id": null,
            "nomisma_id": null,
            "artwork_type_title": "Photograph",
            "artwork_type_id": 2,
            "department_title": "Photography and Media",
            "department_id": "PC-12",
            "artist_id": 30979,
            "artist_title": "Don A. DuBroff",
            "alt_artist_ids": [],
            "artist_ids": [
                30979
            ],
            "artist_titles": [
                "Don A. DuBroff"
            ],
            "category_ids": [
                "PC-12"
            ],
            "category_titles": [
                "Photography and Media"
            ],
            "term_titles": [
                "chromogenic color print",
                "Chicago",
                "photographic process",
                "photography",
                "photograph"
            ],
            "style_id": null,
            "style_title": null,
            "alt_style_ids": [],
            "style_ids": [],
            "style_titles": [],
            "classification_id": "TM-558",
            "classification_title": "chromogenic color print",
            "alt_classification_ids": [
                "TM-68",
                "TM-67",
                "TM-240"
            ],
            "classification_ids": [
                "TM-558",
                "TM-68",
                "TM-67",
                "TM-240"
            ],
            "classification_titles": [
                "chromogenic color print",
                "photographic process",
                "photography",
                "photograph"
            ],
            "subject_id": "TM-12075",
            "alt_subject_ids": [],
            "subject_ids": [
                "TM-12075"
            ],
            "subject_titles": [
                "Chicago"
            ],
            "material_id": null,
            "alt_material_ids": [],
            "material_ids": [],
            "material_titles": [],
            "technique_id": null,
            "alt_technique_ids": [],
            "technique_ids": [],
            "technique_titles": [],
            "theme_titles": [],
            "image_id": "99837c39-af9d-a080-63c0-b34268f41674",
            "alt_image_ids": [],
            "document_ids": [],
            "sound_ids": [],
            "video_ids": [],
            "text_ids": [],
            "section_ids": [],
            "section_titles": [],
            "site_ids": [],
            "suggest_autocomplete_all": [
                {
                    "input": [
                        "1984.415"
                    ],
                    "contexts": {
                        "groupings": [
                            "accession"
                        ]
                    }
                },
                {
                    "input": [
                        "Our Lady of Sorrows Basilica, Chicago"
                    ],
                    "weight": 25,
                    "contexts": {
                        "groupings": [
                            "title"
                        ]
                    }
                }
            ],
            "source_updated_at": "2022-09-21T19:19:06-05:00",
            "updated_at": "2024-06-30T23:28:52-05:00",
            "timestamp": "2024-07-01T00:23:24-05:00"
        },
        {
            "id": 102469,
            "api_model": "artworks",
            "api_link": "https://api.artic.edu/api/v1/artworks/102469",
            "is_boosted": false,
            "title": "Our Lady of Sorrows Basilica, Chicago",
            "alt_titles": null,
            "thumbnail": {
                "lqip": "data:image/gif;base64,R0lGODlhBgAFAPQAAFlUS2ddVGRiVm1kWn1nVX5mWHJoX2lsam97fXJyc39/foBuVoRpWYRtXYZ1W4BwX4JybYJ4bJV9YYd4dYp7cYF4eJSBc5+Kc5aNe5mOfaCLa6OLdMCZbsKdcgAAAAAAACH5BAAAAAAALAAAAAAGAAUAAAUYoAJNVYJQBdMYh0UEwxNtwOIIVyZxnYaFADs=",
                "width": 3000,
                "height": 2384,
                "alt_text": "A work made of chromogenic print."
            },
            "main_reference_number": "1984.414",
            "has_not_been_viewed_much": true,
            "boost_rank": null,
            "date_start": 1981,
            "date_end": 1981,
            "date_display": "1981",
            "date_qualifier_title": "Made",
            "date_qualifier_id": 4,
            "artist_display": "Don A. DuBroff\nAmerican, born 1951",
            "place_of_origin": "United States",
            "description": null,
            "short_description": null,
            "dimensions": "Image: 38 × 48.2 cm (15 × 19 in.); Paper: 40.6 × 50.7 cm (16 × 20 in.)",
            "dimensions_detail": [
                {
                    "depth": null,
                    "width": 48,
                    "height": 38,
                    "diameter": null,
                    "clarification": "Image"
                },
                {
                    "depth": null,
                    "width": 50,
                    "height": 40,
                    "diameter": null,
                    "clarification": "Paper"
                }
            ],
            "medium_display": "Chromogenic print",
            "inscriptions": null,
            "credit_line": "Purchased with funds provided by Jonas Dovydenas and Illinois Arts Council Matching Grant",
            "catalogue_display": null,
            "publication_history": null,
            "exhibition_history": null,
            "provenance_text": null,
            "edition": null,
            "publishing_verification_level": "Web Basic",
            "internal_department_id": 20,
            "fiscal_year": null,
            "fiscal_year_deaccession": null,
            "is_public_domain": false,
            "is_zoomable": false,
            "max_zoom_window_size": 843,
            "copyright_notice": null,
            "has_multimedia_resources": false,
            "has_educational_resources": false,
            "has_advanced_imaging": false,
            "colorfulness": 36.2933,
            "color": {
                "h": 38,
                "l": 51,
                "s": 57,
                "percentage": 0.002832811033798977,
                "population": 16
            },
            "latitude": null,
            "longitude": null,
            "latlon": null,
            "is_on_view": false,
            "on_loan_display": null,
            "gallery_title": null,
            "gallery_id": null,
            "nomisma_id": null,
            "artwork_type_title": "Photograph",
            "artwork_type_id": 2,
            "department_title": "Photography and Media",
            "department_id": "PC-12",
            "artist_id": 30979,
            "artist_title": "Don A. DuBroff",
            "alt_artist_ids": [],
            "artist_ids": [
                30979
            ],
            "artist_titles": [
                "Don A. DuBroff"
            ],
            "category_ids": [
                "PC-12"
            ],
            "category_titles": [
                "Photography and Media"
            ],
            "term_titles": [
                "chromogenic color print",
                "Chicago",
                "photographic process",
                "photography",
                "photograph"
            ],
            "style_id": null,
            "style_title": null,
            "alt_style_ids": [],
            "style_ids": [],
            "style_titles": [],
            "classification_id": "TM-558",
            "classification_title": "chromogenic color print",
            "alt_classification_ids": [
                "TM-68",
                "TM-67",
                "TM-240"
            ],
            "classification_ids": [
                "TM-558",
                "TM-68",
                "TM-67",
                "TM-240"
            ],
            "classification_titles": [
                "chromogenic color print",
                "photographic process",
                "photography",
                "photograph"
            ],
            "subject_id": "TM-12075",
            "alt_subject_ids": [],
            "subject_ids": [
                "TM-12075"
            ],
            "subject_titles": [
                "Chicago"
            ],
            "material_id": null,
            "alt_material_ids": [],
            "material_ids": [],
            "material_titles": [],
            "technique_id": null,
            "alt_technique_ids": [],
            "technique_ids": [],
            "technique_titles": [],
            "theme_titles": [],
            "image_id": "99837c39-af9d-a080-63c0-b34268f41674",
            "alt_image_ids": [],
            "document_ids": [],
            "sound_ids": [],
            "video_ids": [],
            "text_ids": [],
            "section_ids": [],
            "section_titles": [],
            "site_ids": [],
            "suggest_autocomplete_all": [
                {
                    "input": [
                        "1984.414"
                    ],
                    "contexts": {
                        "groupings": [
                            "accession"
                        ]
                    }
                },
                {
                    "input": [
                        "Our Lady of Sorrows Basilica, Chicago"
                    ],
                    "weight": 48,
                    "contexts": {
                        "groupings": [
                            "title"
                        ]
                    }
                }
            ],
            "source_updated_at": "2022-09-21T19:19:06-05:00",
            "updated_at": "2024-06-30T23:28:52-05:00",
            "timestamp": "2024-07-01T00:23:24-05:00"
        },
        {
            "id": 102468,
            "api_model": "artworks",
            "api_link": "https://api.artic.edu/api/v1/artworks/102468",
            "is_boosted": false,
            "title": "Notre Dame Church, Chicago",
            "alt_titles": null,
            "thumbnail": {
                "lqip": "data:image/gif;base64,R0lGODlhBAAFAPQAAGpTP3lWMnhWM3lZMVpOR15RRl1idJReJ49fMJZnLY9iNJFuP6FxLaBxN6x9S5V8Y6ODYnF3gFeBrnaHxwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAAAAAAALAAAAAAEAAUAAAUR4OI0ADRFySMZCoMcQTEIRAgAOw==",
                "width": 2406,
                "height": 3000,
                "alt_text": "A work made of chromogenic print."
            },
            "main_reference_number": "1984.413",
            "has_not_been_viewed_much": true,
            "boost_rank": null,
            "date_start": 1983,
            "date_end": 1983,
            "date_display": "1983",
            "date_qualifier_title": "Made",
            "date_qualifier_id": 4,
            "artist_display": "Don A. DuBroff\nAmerican, born 1951",
            "place_of_origin": "United States",
            "description": null,
            "short_description": null,
            "dimensions": "Image: 48.1 × 38 cm (18 15/16 × 15 in.); Paper: 50.7 × 40.6 cm (20 × 16 in.)",
            "dimensions_detail": [
                {
                    "depth": null,
                    "width": 38,
                    "height": 48,
                    "diameter": null,
                    "clarification": "Image"
                },
                {
                    "depth": null,
                    "width": 40,
                    "height": 50,
                    "diameter": null,
                    "clarification": "Paper"
                }
            ],
            "medium_display": "Chromogenic print",
            "inscriptions": null,
            "credit_line": "Purchased with funds provided by Jonas Dovydenas",
            "catalogue_display": null,
            "publication_history": null,
            "exhibition_history": null,
            "provenance_text": null,
            "edition": null,
            "publishing_verification_level": "Web Basic",
            "internal_department_id": 20,
            "fiscal_year": null,
            "fiscal_year_deaccession": null,
            "is_public_domain": false,
            "is_zoomable": false,
            "max_zoom_window_size": 843,
            "copyright_notice": null,
            "has_multimedia_resources": false,
            "has_educational_resources": false,
            "has_advanced_imaging": false,
            "colorfulness": 66.7603,
            "color": {
                "h": 195,
                "l": 48,
                "s": 65,
                "percentage": 0.01049669692649942,
                "population": 93
            },
            "latitude": null,
            "longitude": null,
            "latlon": null,
            "is_on_view": false,
            "on_loan_display": null,
            "gallery_title": null,
            "gallery_id": null,
            "nomisma_id": null,
            "artwork_type_title": "Photograph",
            "artwork_type_id": 2,
            "department_title": "Photography and Media",
            "department_id": "PC-12",
            "artist_id": 30979,
            "artist_title": "Don A. DuBroff",
            "alt_artist_ids": [],
            "artist_ids": [
                30979
            ],
            "artist_titles": [
                "Don A. DuBroff"
            ],
            "category_ids": [
                "PC-12"
            ],
            "category_titles": [
                "Photography and Media"
            ],
            "term_titles": [
                "chromogenic color print",
                "Chicago",
                "photographic process",
                "photography",
                "photograph"
            ],
            "style_id": null,
            "style_title": null,
            "alt_style_ids": [],
            "style_ids": [],
            "style_titles": [],
            "classification_id": "TM-558",
            "classification_title": "chromogenic color print",
            "alt_classification_ids": [
                "TM-68",
                "TM-67",
                "TM-240"
            ],
            "classification_ids": [
                "TM-558",
                "TM-68",
                "TM-67",
                "TM-240"
            ],
            "classification_titles": [
                "chromogenic color print",
                "photographic process",
                "photography",
                "photograph"
            ],
            "subject_id": "TM-12075",
            "alt_subject_ids": [],
            "subject_ids": [
                "TM-12075"
            ],
            "subject_titles": [
                "Chicago"
            ],
            "material_id": null,
            "alt_material_ids": [],
            "material_ids": [],
            "material_titles": [],
            "technique_id": null,
            "alt_technique_ids": [],
            "technique_ids": [],
            "technique_titles": [],
            "theme_titles": [],
            "image_id": "99ceb6af-1f61-9911-9bda-8d8ac1ac3db4",
            "alt_image_ids": [],
            "document_ids": [],
            "sound_ids": [],
            "video_ids": [],
            "text_ids": [],
            "section_ids": [],
            "section_titles": [],
            "site_ids": [],
            "suggest_autocomplete_all": [
                {
                    "input": [
                        "1984.413"
                    ],
                    "contexts": {
                        "groupings": [
                            "accession"
                        ]
                    }
                },
                {
                    "input": [
                        "Notre Dame Church, Chicago"
                    ],
                    "weight": 10,
                    "contexts": {
                        "groupings": [
                            "title"
                        ]
                    }
                }
            ],
            "source_updated_at": "2022-09-21T19:19:06-05:00",
            "updated_at": "2024-06-30T23:28:52-05:00",
            "timestamp": "2024-07-01T00:23:24-05:00"
        },
        {
            "id": 102467,
            "api_model": "artworks",
            "api_link": "https://api.artic.edu/api/v1/artworks/102467",
            "is_boosted": false,
            "title": "Notre Dame Church, Chicago",
            "alt_titles": null,
            "thumbnail": {
                "lqip": "data:image/gif;base64,R0lGODlhBAAFAPQAAGpTP3lWMnhWM3lZMVpOR15RRl1idJReJ49fMJZnLY9iNJFuP6FxLaBxN6x9S5V8Y6ODYnF3gFeBrnaHxwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAAAAAAALAAAAAAEAAUAAAUR4OI0ADRFySMZCoMcQTEIRAgAOw==",
                "width": 2406,
                "height": 3000,
                "alt_text": "A work made of chromogenic print."
            },
            "main_reference_number": "1984.412",
            "has_not_been_viewed_much": true,
            "boost_rank": null,
            "date_start": 1983,
            "date_end": 1983,
            "date_display": "1983",
            "date_qualifier_title": "Made",
            "date_qualifier_id": 4,
            "artist_display": "Don A. DuBroff\nAmerican, born 1951",
            "place_of_origin": "United States",
            "description": null,
            "short_description": null,
            "dimensions": "Image: 48.1 × 38.1 cm (18 15/16 × 15 in.); Paper: 50.7 × 40.6 cm (20 × 16 in.)",
            "dimensions_detail": [
                {
                    "depth": null,
                    "width": 38,
                    "height": 48,
                    "diameter": null,
                    "clarification": "Image"
                },
                {
                    "depth": null,
                    "width": 40,
                    "height": 50,
                    "diameter": null,
                    "clarification": "Paper"
                }
            ],
            "medium_display": "Chromogenic print",
            "inscriptions": null,
            "credit_line": "Purchased with funds provided by Jonas Dovydenas and Illinois Arts Council Matching Grant",
            "catalogue_display": null,
            "publication_history": null,
            "exhibition_history": null,
            "provenance_text": null,
            "edition": null,
            "publishing_verification_level": "Web Basic",
            "internal_department_id": 20,
            "fiscal_year": null,
            "fiscal_year_deaccession": null,
            "is_public_domain": false,
            "is_zoomable": false,
            "max_zoom_window_size": 843,
            "copyright_notice": null,
            "has_multimedia_resources": false,
            "has_educational_resources": false,
            "has_advanced_imaging": false,
            "colorfulness": 66.7603,
            "color": {
                "h": 195,
                "l": 48,
                "s": 65,
                "percentage": 0.01049669692649942,
                "population": 93
            },
            "latitude": null,
            "longitude": null,
            "latlon": null,
            "is_on_view": false,
            "on_loan_display": null,
            "gallery_title": null,
            "gallery_id": null,
            "nomisma_id": null,
            "artwork_type_title": "Photograph",
            "artwork_type_id": 2,
            "department_title": "Photography and Media",
            "department_id": "PC-12",
            "artist_id": 30979,
            "artist_title": "Don A. DuBroff",
            "alt_artist_ids": [],
            "artist_ids": [
                30979
            ],
            "artist_titles": [
                "Don A. DuBroff"
            ],
            "category_ids": [
                "PC-12"
            ],
            "category_titles": [
                "Photography and Media"
            ],
            "term_titles": [
                "chromogenic color print",
                "Chicago",
                "photographic process",
                "photography",
                "photograph"
            ],
            "style_id": null,
            "style_title": null,
            "alt_style_ids": [],
            "style_ids": [],
            "style_titles": [],
            "classification_id": "TM-558",
            "classification_title": "chromogenic color print",
            "alt_classification_ids": [
                "TM-68",
                "TM-67",
                "TM-240"
            ],
            "classification_ids": [
                "TM-558",
                "TM-68",
                "TM-67",
                "TM-240"
            ],
            "classification_titles": [
                "chromogenic color print",
                "photographic process",
                "photography",
                "photograph"
            ],
            "subject_id": "TM-12075",
            "alt_subject_ids": [],
            "subject_ids": [
                "TM-12075"
            ],
            "subject_titles": [
                "Chicago"
            ],
            "material_id": null,
            "alt_material_ids": [],
            "material_ids": [],
            "material_titles": [],
            "technique_id": null,
            "alt_technique_ids": [],
            "technique_ids": [],
            "technique_titles": [],
            "theme_titles": [],
            "image_id": "99ceb6af-1f61-9911-9bda-8d8ac1ac3db4",
            "alt_image_ids": [],
            "document_ids": [],
            "sound_ids": [],
            "video_ids": [],
            "text_ids": [],
            "section_ids": [],
            "section_titles": [],
            "site_ids": [],
            "suggest_autocomplete_all": [
                {
                    "input": [
                        "1984.412"
                    ],
                    "contexts": {
                        "groupings": [
                            "accession"
                        ]
                    }
                },
                {
                    "input": [
                        "Notre Dame Church, Chicago"
                    ],
                    "weight": 21,
                    "contexts": {
                        "groupings": [
                            "title"
                        ]
                    }
                }
            ],
            "source_updated_at": "2022-09-21T19:19:06-05:00",
            "updated_at": "2024-06-30T23:28:52-05:00",
            "timestamp": "2024-07-01T00:23:24-05:00"
        },
        {
            "id": 102466,
            "api_model": "artworks",
            "api_link": "https://api.artic.edu/api/v1/artworks/102466",
            "is_boosted": false,
            "title": "St. Nicolas Ukrainian Cathedral, Chicago",
            "alt_titles": null,
            "thumbnail": {
                "lqip": "data:image/gif;base64,R0lGODlhBAAFAPQAADAwMzhVVk1GQFtVTmtjVVlrc099fGltcINuYot7bqeEZF12iUp1kVZ/l06JhmeVjGiBkFSvqo+XlaikowAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAAAAAAALAAAAAAEAAUAAAURICAEzkA8UaIgxiQVx8I0UAgAOw==",
                "width": 642,
                "height": 768,
                "alt_text": "A work made of chromogenic print."
            },
            "main_reference_number": "1984.411",
            "has_not_been_viewed_much": true,
            "boost_rank": null,
            "date_start": 1979,
            "date_end": 1979,
            "date_display": "1979",
            "date_qualifier_title": "Made",
            "date_qualifier_id": 4,
            "artist_display": "Don A. DuBroff\nAmerican, born 1951",
            "place_of_origin": "United States",
            "description": null,
            "short_description": null,
            "dimensions": "Image: 45.3 × 38.1 cm (17 7/8 × 15 in.); Paper: 50.7 × 40.6 cm (20 × 16 in.)",
            "dimensions_detail": [
                {
                    "depth": null,
                    "width": 38,
                    "height": 45,
                    "diameter": null,
                    "clarification": "Image"
                },
                {
                    "depth": null,
                    "width": 40,
                    "height": 50,
                    "diameter": null,
                    "clarification": "Paper"
                }
            ],
            "medium_display": "Chromogenic print",
            "inscriptions": null,
            "credit_line": "Purchased with funds provided by Jonas Dovydenas",
            "catalogue_display": null,
            "publication_history": null,
            "exhibition_history": null,
            "provenance_text": null,
            "edition": null,
            "publishing_verification_level": "Web Basic",
            "internal_department_id": 20,
            "fiscal_year": null,
            "fiscal_year_deaccession": null,
            "is_public_domain": false,
            "is_zoomable": false,
            "max_zoom_window_size": 843,
            "copyright_notice": null,
            "has_multimedia_resources": false,
            "has_educational_resources": false,
            "has_advanced_imaging": false,
            "colorfulness": 47.0371,
            "color": {
                "h": 195,
                "l": 55,
                "s": 39,
                "percentage": 0.0919100340808526,
                "population": 781
            },
            "latitude": null,
            "longitude": null,
            "latlon": null,
            "is_on_view": false,
            "on_loan_display": null,
            "gallery_title": null,
            "gallery_id": null,
            "nomisma_id": null,
            "artwork_type_title": "Photograph",
            "artwork_type_id": 2,
            "department_title": "Photography and Media",
            "department_id": "PC-12",
            "artist_id": 30979,
            "artist_title": "Don A. DuBroff",
            "alt_artist_ids": [],
            "artist_ids": [
                30979
            ],
            "artist_titles": [
                "Don A. DuBroff"
            ],
            "category_ids": [
                "PC-12"
            ],
            "category_titles": [
                "Photography and Media"
            ],
            "term_titles": [
                "chromogenic color print",
                "Chicago",
                "photographic process",
                "photography",
                "photograph"
            ],
            "style_id": null,
            "style_title": null,
            "alt_style_ids": [],
            "style_ids": [],
            "style_titles": [],
            "classification_id": "TM-558",
            "classification_title": "chromogenic color print",
            "alt_classification_ids": [
                "TM-68",
                "TM-67",
                "TM-240"
            ],
            "classification_ids": [
                "TM-558",
                "TM-68",
                "TM-67",
                "TM-240"
            ],
            "classification_titles": [
                "chromogenic color print",
                "photographic process",
                "photography",
                "photograph"
            ],
            "subject_id": "TM-12075",
            "alt_subject_ids": [],
            "subject_ids": [
                "TM-12075"
            ],
            "subject_titles": [
                "Chicago"
            ],
            "material_id": null,
            "alt_material_ids": [],
            "material_ids": [],
            "material_titles": [],
            "technique_id": null,
            "alt_technique_ids": [],
            "technique_ids": [],
            "technique_titles": [],
            "theme_titles": [],
            "image_id": "11834aed-2bf6-dc5c-4481-3436a1a2c0d4",
            "alt_image_ids": [],
            "document_ids": [],
            "sound_ids": [],
            "video_ids": [],
            "text_ids": [],
            "section_ids": [],
            "section_titles": [],
            "site_ids": [],
            "suggest_autocomplete_all": [
                {
                    "input": [
                        "1984.411"
                    ],
                    "contexts": {
                        "groupings": [
                            "accession"
                        ]
                    }
                },
                {
                    "input": [
                        "St. Nicolas Ukrainian Cathedral, Chicago"
                    ],
                    "weight": 31,
                    "contexts": {
                        "groupings": [
                            "title"
                        ]
                    }
                }
            ],
            "source_updated_at": "2022-09-21T19:19:06-05:00",
            "updated_at": "2024-06-30T23:28:52-05:00",
            "timestamp": "2024-07-01T00:23:24-05:00"
        },
        {
            "id": 102465,
            "api_model": "artworks",
            "api_link": "https://api.artic.edu/api/v1/artworks/102465",
            "is_boosted": false,
            "title": "St. Nicolas Ukrainian Cathedral, Chicago",
            "alt_titles": null,
            "thumbnail": {
                "lqip": "data:image/gif;base64,R0lGODlhBAAFAPQAAC1GUjNNVR5RaSddZChVaBZtejBiazpsc1NPT1tRSktXXFNXXWBXU2liXUdfZ0xmd1Focot0X46Ben6CigAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAAAAAAALAAAAAAEAAUAAAUR4IIER5MMhRQxxgQByiMQTggAOw==",
                "width": 2538,
                "height": 3000,
                "alt_text": "A work made of chromogenic print."
            },
            "main_reference_number": "1984.410",
            "has_not_been_viewed_much": true,
            "boost_rank": null,
            "date_start": 1979,
            "date_end": 1979,
            "date_display": "1979",
            "date_qualifier_title": "Made",
            "date_qualifier_id": 4,
            "artist_display": "Don A. DuBroff\nAmerican, born 1951",
            "place_of_origin": "United States",
            "description": null,
            "short_description": null,
            "dimensions": "Image: 45.3 × 38 cm (17 7/8 × 15 in.); Paper: 50.7 × 40.6 cm (20 × 16 in.)",
            "dimensions_detail": [
                {
                    "depth": null,
                    "width": 38,
                    "height": 45,
                    "diameter": null,
                    "clarification": "Image"
                },
                {
                    "depth": null,
                    "width": 40,
                    "height": 50,
                    "diameter": null,
                    "clarification": "Paper"
                }
            ],
            "medium_display": "Chromogenic print",
            "inscriptions": null,
            "credit_line": "Purchased with funds provided by Reva and David Logan and Illinois Arts Council Matching Grant",
            "catalogue_display": null,
            "publication_history": null,
            "exhibition_history": null,
            "provenance_text": null,
            "edition": null,
            "publishing_verification_level": "Web Basic",
            "internal_department_id": 20,
            "fiscal_year": null,
            "fiscal_year_deaccession": null,
            "is_public_domain": false,
            "is_zoomable": false,
            "max_zoom_window_size": 843,
            "copyright_notice": null,
            "has_multimedia_resources": false,
            "has_educational_resources": false,
            "has_advanced_imaging": false,
            "colorfulness": 48.0512,
            "color": {
                "h": 200,
                "l": 44,
                "s": 58,
                "percentage": 0.00035730109048292814,
                "population": 3
            },
            "latitude": null,
            "longitude": null,
            "latlon": null,
            "is_on_view": false,
            "on_loan_display": null,
            "gallery_title": null,
            "gallery_id": null,
            "nomisma_id": null,
            "artwork_type_title": "Photograph",
            "artwork_type_id": 2,
            "department_title": "Photography and Media",
            "department_id": "PC-12",
            "artist_id": 30979,
            "artist_title": "Don A. DuBroff",
            "alt_artist_ids": [],
            "artist_ids": [
                30979
            ],
            "artist_titles": [
                "Don A. DuBroff"
            ],
            "category_ids": [
                "PC-12"
            ],
            "category_titles": [
                "Photography and Media"
            ],
            "term_titles": [
                "chromogenic color print",
                "Chicago",
                "photographic process",
                "photography",
                "photograph"
            ],
            "style_id": null,
            "style_title": null,
            "alt_style_ids": [],
            "style_ids": [],
            "style_titles": [],
            "classification_id": "TM-558",
            "classification_title": "chromogenic color print",
            "alt_classification_ids": [
                "TM-68",
                "TM-67",
                "TM-240"
            ],
            "classification_ids": [
                "TM-558",
                "TM-68",
                "TM-67",
                "TM-240"
            ],
            "classification_titles": [
                "chromogenic color print",
                "photographic process",
                "photography",
                "photograph"
            ],
            "subject_id": "TM-12075",
            "alt_subject_ids": [],
            "subject_ids": [
                "TM-12075"
            ],
            "subject_titles": [
                "Chicago"
            ],
            "material_id": null,
            "alt_material_ids": [],
            "material_ids": [],
            "material_titles": [],
            "technique_id": null,
            "alt_technique_ids": [],
            "technique_ids": [],
            "technique_titles": [],
            "theme_titles": [],
            "image_id": "5d647f14-1c0d-2469-5241-c926d380baf8",
            "alt_image_ids": [],
            "document_ids": [],
            "sound_ids": [],
            "video_ids": [],
            "text_ids": [],
            "section_ids": [],
            "section_titles": [],
            "site_ids": [],
            "suggest_autocomplete_all": [
                {
                    "input": [
                        "1984.410"
                    ],
                    "contexts": {
                        "groupings": [
                            "accession"
                        ]
                    }
                },
                {
                    "input": [
                        "St. Nicolas Ukrainian Cathedral, Chicago"
                    ],
                    "weight": 1,
                    "contexts": {
                        "groupings": [
                            "title"
                        ]
                    }
                }
            ],
            "source_updated_at": "2022-09-21T19:19:06-05:00",
            "updated_at": "2024-06-30T23:28:52-05:00",
            "timestamp": "2024-07-01T00:23:24-05:00"
        }
    ],
    "info": {
        "license_text": "The `description` field in this response is licensed under a Creative Commons Attribution 4.0 Generic License (CC-By) and the Terms and Conditions of artic.edu. All other data in this response is licensed under a Creative Commons Zero (CC0) 1.0 designation and the Terms and Conditions of artic.edu.",
        "license_links": [
            "https://creativecommons.org/publicdomain/zero/1.0/",
            "https://www.artic.edu/terms"
        ],
        "version": "1.10"
    },
    "config": {
        "iiif_url": "https://www.artic.edu/iiif/2",
        "website_url": "http://www.artic.edu"
    }
}

export default mockData;