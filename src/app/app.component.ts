import { Component, SimpleChanges } from '@angular/core';
import { APP_CONST } from './core/utilities/constants/app';
import { CommonAPIService } from './core/services/common-api.service';
import { ArtworkCollection } from './core/interface/artworks.interface';
import { ArtworkCollectionsService } from './core/services/artwork-collections.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = APP_CONST.TITLE.NAME;

  loadDataInProgress: boolean = true;

  artworkLists: any;
  iniCollections: ArtworkCollection[] = [];
  collections: ArtworkCollection[] = [];
  category:any = '';

  options: any = [];

  count = 210;
  page = 1;
  perPage = APP_CONST.PAGINATION.DEFAULT_PAGE_SIZE;

  constructor(
    private commonApiService: CommonAPIService, 
    private artworkCollectionsService: ArtworkCollectionsService,
  ){}

  ngOnInit(){
    this.getArtWork(this.page);
  }

  prevPage() {
      this.page--;
      this.getArtWork(this.page);
  }

  nextPage() {
      this.page++;
      this.getArtWork(this.page);
  }

  goToPage(n: any) {
      console.log('goToPage: ', this.category)
      this.page = n;
      this.getArtWork(this.page);
  }

  getArtWork(page: number = 1){
    this.loadDataInProgress = true;
    this.commonApiService.getArtWorks(page, this.perPage).subscribe({
      next: (response) => {
        this.artworkCollectionsService.setArtWorkCollections = response;
        this.artworkLists = this.artworkCollectionsService.getArtWorkCollections;
        this.count = this.artworkLists.pagination.total;
        this.collections = this.artworkCollectionsService.manageCollection(this.artworkLists.data);
        this.options = this.artworkCollectionsService.getFilterLists(this.collections);

        if(this.category){
          this.sorting(this.category);
        }

        this.loadDataInProgress = false;
      },
      error: (err) => {
        this.loadDataInProgress = true;
      }
    })
  }

  onSorting(event: any){
    this.category = event.value;
    this.sorting(event.value);
  }

  sorting(category: any){
    this.category = category;
    this.collections.sort((a: any, b: any) => {
      if (this.category === 'Name') {
        return a.title.localeCompare(b.title);
      } else if (this.category === 'Artist') {
        return a.artist_title.localeCompare(b.artist_title);
      } else if (this.category === 'Date') {
        return a.date_start - b.date_start;
      }
      return 0; 
    });
  }

  onFilter(event: any){
    const styles = event;
    this.collections = this.artworkCollectionsService.getManageCollections;
    if(styles.length > 0){
      this.collections = this.collections.filter((item: any) => styles.includes(item.style_title));
      if(this.category){
        this.sorting(this.category);
      }
    }
  }
  
}
