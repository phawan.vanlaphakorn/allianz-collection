import { Injectable } from "@angular/core";
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from "@angular/common/http";
import { catchError, Observable, switchMap, throwError } from "rxjs";

@Injectable({
    providedIn: 'root',
})

export class InterceptorService implements HttpInterceptor {

    constructor(){}

    intercept(
        request: HttpRequest<any>, 
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        request = request.clone({
            setHeaders:  {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
                'Access-Control-Allow-Headers':'Content-Type, Authorization, X-Requested-With,Authentication',
                // 'Authentication': `Bearer ${'DUMMY'}`,
                // "X-Claims": 'dummytoken'
              }
        });

        return next.handle(request).pipe(
            catchError((error: HttpErrorResponse) => {
                if(
                    (error && error.status == 404) ||
                    error.status == 401 ||
                    error.status == 500 ||
                    error.status == 400
                ){
                    console.log(error);
                    return throwError(()=> error)
                }else{
                    console.log(error);
                    return throwError(()=> error)
                }
            })
        );
    }
}