export const APP_CONST = {
    TITLE: {
        NAME: 'ART COLLECTION',
    },
    PAGINATION: {
        DEFAULT_PAGE_SIZE : 8
    }
}