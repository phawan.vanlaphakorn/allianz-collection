import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { environment } from "src/environments/environment";
import { Endpoint } from "../utilities/constants/endpoints";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from 'rxjs/operators';
import { Router } from "@angular/router";
import { ArtworkCollectionsService } from "./artwork-collections.service";

@Injectable({
    providedIn: 'root',
})

export class CommonAPIService {
    constructor(
        private http: HttpClient,
        private router: Router,
        private artworkCollectionsService: ArtworkCollectionsService,

    ){}

    getArtWorks(page: number = 1, limit: number = 8): Observable<any>{
        let pathName = Endpoint.Artworks.GetArtWorks;
        return this.http.get<any>(environment.apiUrl + pathName + '?page=' + page + '&limit=' + limit);
    }
}