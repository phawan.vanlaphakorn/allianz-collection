import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})

export class ArtworkCollectionsService {
    private _artworks: any;
    private _collections: any;

    constructor(){}

    set setArtWorkCollections(artworks: any){
        this._artworks = artworks
    }

    set setManageCollections(collections: any){
        this._collections = collections
    }

    get getArtWorkCollections(){
        return this._artworks;
    }

    get getManageCollections(){
        return this._collections;
    }

    manageCollection(artWorkList: any){
        this._collections = artWorkList.map((collection: any) => {
          let locate: string = '';
          if(collection.date_start == null && collection.date_end == null){
            locate = collection.place_of_origin;
          }else if(collection.date_start !== null && collection.date_end == null){
            locate = collection.place_of_origin + ' (' + collection.date_start + ')';
          }else {
            locate = collection.place_of_origin + ' (' + collection.date_start + ' - ' + collection.date_end + ')';
          }
    
          let image_id: string = '';
    
          if(collection.image_id){
            image_id = 'https://www.artic.edu/iiif/2/'+ collection.image_id + '/full/600,/0/default.jpg';
          }
    
          return {
            image_id: image_id,
            title: collection.title,
            alt_titles: collection.alt_titles ?? '',
            artist_title: collection.artist_title ?? '-',
            style_title: collection.style_title ?? 'no title',
            location: locate,
            date_start: collection.date_start,
            material_titles: collection.material_titles,
          }
        })
        return this._collections;
    }

    getFilterLists(collections: any){
        const itemCounts = collections.reduce((countMap: any, obj: any) => {
          const { style_title } = obj;
          countMap[style_title] = (countMap[style_title] || 0) + 1;
          return countMap;
        }, {});
    
        const mappedData = Object.entries(itemCounts).map(([style_title, count]) => {
          return {
            id: style_title,
            value: `${style_title} (${count})`
          }
        });
    
        return mappedData;
    }
}