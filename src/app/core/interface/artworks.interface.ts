export interface ArtworkCollection {
    image_id?: String;
    title?: String;
    alt_titles?: String;
    artist_title?: String;
    style_title?: String;
    location?: String;
    date_start?: String;
    material_titles?: any;
}