import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CollectionsComponent } from './collections.component';
import { ShareModule } from 'src/app/share/share.module';

describe('CollectionsComponent', () => {
  let component: CollectionsComponent;
  let fixture: ComponentFixture<CollectionsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CollectionsComponent],
      imports: [ShareModule]
    });
    fixture = TestBed.createComponent(CollectionsComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
