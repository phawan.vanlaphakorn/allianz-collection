import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Asset } from '../../core/utilities/constants/asset';
import { ArtworkCollection } from 'src/app/core/interface/artworks.interface';

@Component({
  selector: 'app-collections',
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.scss']
})
export class CollectionsComponent {

  @Input() collections: ArtworkCollection[] = [];
  @Input() options: any = [];
  @Input() category: any = '';
  @Output() onFilter = new EventEmitter();
  @Output() onSorting = new EventEmitter();
  sortLists: any = [
      'Name',
      'Artist',
      'Date',
  ];

  handleImageError(index: number){
    if(this.collections && this.collections.length > 0){
      this.collections[index].image_id = Asset.Image.Placeholder;

    }
  }

  filter(event: any){
    this.onFilter.emit(event);
  }

  sorting(event: any){
    this.category = event.value;
    this.onSorting.emit(event);
  }
}
