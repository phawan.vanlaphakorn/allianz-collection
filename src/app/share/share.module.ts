import { HttpClientModule } from "@angular/common/http";
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { NxCardModule } from "@aposin/ng-aquila/card";
import { NxDropdownModule } from "@aposin/ng-aquila/dropdown";
import { NxFormfieldModule } from "@aposin/ng-aquila/formfield";
import { NxGridModule } from "@aposin/ng-aquila/grid";
import { NxImageModule } from "@aposin/ng-aquila/image";
import { NxPaginationModule } from "@aposin/ng-aquila/pagination";
import { NxSpinnerModule } from "@aposin/ng-aquila/spinner";
import { NxHeadlineModule } from '@aposin/ng-aquila/headline';

@NgModule({
    declarations: [],
    imports: [
        HttpClientModule,
        NxCardModule,
        NxGridModule,
        NxHeadlineModule,
        NxImageModule,
        NxPaginationModule,
        NxFormfieldModule,
        NxDropdownModule,
        NxSpinnerModule
    ],
    exports: [
        NxCardModule,
        NxHeadlineModule,
        NxGridModule,
        NxImageModule,
        NxPaginationModule,
        NxFormfieldModule,
        NxDropdownModule,
        NxSpinnerModule
    ], 
    providers : [HttpClientModule]
})

export class ShareModule {}